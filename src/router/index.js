import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/second",
    name: "Second",
    component: () =>
      import(/* webpackChunkName: "second" */ "../views/Second.vue"),
  },
  {
    path: "/third",
    name: "Third",
    component: () =>
      import(/* webpackChunkName: "third" */ "../views/Third.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
